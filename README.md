# **Vue Guild Wars Builds**

[![](https://img.shields.io/npm/v/vue-gw-builds.svg?style=flat-square&color=cb3837&logo=npm&logoColor=ffffff)](https://www.npmjs.com/package/vue-dependencies-badges)
[![](https://img.shields.io/github/license/ewilan-riviere/vuepress-theme-useweb.svg?style=flat-square&color=f05032&logo=git&logoColor=ffffff)](https://github.com/ewilan-riviere/dependencies-badges/blob/master/LICENSE)

[![node.js](https://img.shields.io/static/v1?label=Node.js&message=v11.15&color=339933&style=flat-square&logo=node.js&logoColor=ffffff)](https://nodejs.org/en/)
[![vue.js](https://img.shields.io/static/v1?label=Vue.js&message=v2.6&color=4FC08D&style=flat-square&logo=vue.js&logoColor=ffffff)](https://vuejs.org/)

Package to easily display [**Guild Wars game**](https://www.guildwars.com) builds into any Vue.js app. It's a Vue wrapper of [**arnapou's gwbbcode**](https://github.com/arnapou/gwbbcode) and this plugin use his API.

>**WARNING**
>
>If, in the future for me and present for you, his API: [**gwbbcode.arnapou.net/gwbbcode/api.js**](https://gwbbcode.arnapou.net/gwbbcode/api.js) is not available anymore, you will have to host it before to use this plugin.
>
> You can find it one the official repository: [**arnapou's gwbbcode**](https://github.com/arnapou/gwbbcode) or on my fork (if official is not available) : [**ewilan-riviere/gwbbcode**](https://github.com/ewilan-riviere/gwbbcode).

![build](public/images/build.png)

## **1. About**

I love Guild Wars game but it's old game and resources about disappear with time. I found the arnapou's work about displaying builds easily without classic [**GWBBCode**](https://www.gwiki.fr/wiki/GwBBCode/Utilisation) which is the reference to display build on phpBB forums when Guild Wars was young. So thanks to him, because he decided to code from scratch a JS API to display easily builds. He wrote the code in 2011 with jQuery, so, today, it's not up-to-date anymore with current standards.

But it's a clear code, without any bugs, and I thought it's not efficiency to always re-write all code with new standards. The original author rewrite original code of GWBBCode because it's very old code just for phpBB forums, I totally understand it, it's great work. It works well today with his API, so I just decide to create a Vue wrapper to use easily his work in any Vue app. I couldn't add the API here because:
- I want to maintain a light package and the API contain all skills pictures (and it's heavy)
- Integrate directly API in this package is not a very good idea, it's better to seperate database from application

But this choice have a cost: the dependency to original API. When I write this, the API is available but one day, maybe, this will not. So if you want to use this package, you will have to host API on any URL and add this URL to importation of this plugin, like the documentation section explain it. You can refer to warning at the top of this file to find the API repository

---

## **2. Features**

If you use feature with `fr` config, you have to use french skill names, if you use `en` config, you have to use english skill names. You can't use differents configs, you have to choose when you import the plugin.

### **2. a. Display a build with skills**

You can display any build with skills like this

```vue
<template>
  <gw-build>
    [build prof=E/W fire=10 air=5 sword=1][searing flames][fireball][incendiary bonds][flare][glowing gaze][fire storm][shock][gale][/build]
  </gw-build>
</template>
```

It's display code template too.

### **2. b. Display with code template**

Just use code template

```vue
<template>
  <gw-build>
    [build=OAAjooIFVM0NdxsQYMWLGnDRB]
  </gw-build>
</template>
```

### **2. c. Display just a skill**

To display a just skill

```vue
<template>
  <gw-build>
    [meteor shower@10] ou [[meteor shower@10]
  </gw-build>
</template>
```

---

## **3. Documentation**

Install it with Yarn or NPM

```bash
yarn add -D vue-gw-builds
```

**OR**

```bash
npm i vue-gw-builds --save-dev
```

Import it, ES6 way, in `main.js` / `app.js` file

```js
import vueGwBuilds from 'vue-gw-builds'

Vue.use(vueGwBuilds, {
  apiUrl: 'http://gwbbcode.arnapou.net/gwbbcode/api.js',
  lang: 'en'
})
```

Use it in a `.vue` file

```vue
<template>
  <div>
    <div>
      Build with skills details (Fr/En syntax available)
      <gw-build>
        [build prof=El/G feu=10 air=5 epe=1][Flammes virulentes][Boule de feu][Liens incendiaires][Eclat de feu][Regard embrasé][Tempête de feu][Décharge électrique][Bourrasque][/build]
      </gw-build>
      <gw-build>
        [build prof=E/W fire=10 air=5 sword=1][searing flames][fireball][incendiary bonds][flare][glowing gaze][fire storm][shock][gale][/build]
      </gw-build>
    </div>
    <div>
      <div>
        Build with template code
      </div>
      <gw-build>
        [build=OAAjooIFVM0NdxsQYMWLGnDRB]
      </gw-build>
    </div>
    <div>
      Display just a skill with skills points (here Meteor Shower is Fire Magic,
      so '@10' indicate 10 points in this attribute)
      <gw-build>
        [pluie de météores@10] ou [[pluie de météores@10]
      </gw-build>
      <gw-build>
        [meteor shower@10] ou [[meteor shower@10]
      </gw-build>
    </div>
  </div>
</template>
```

---

## **License**

[**MIT**](https://github.com/ewilan-riviere/dependencies-badges/blob/master/LICENSE) @ Liu Pi, Ouroboros, arnapou, ewilan-riviere
