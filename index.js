import GwBbCode from './lib/gw-bb-code.vue'

export default {
  install(Vue, options) {
    if (options) {
      Vue.prototype.$apiUrl = options.apiUrl
      Vue.prototype.$lang = options.lang
    }
    Vue.component('gw-build', GwBbCode)
  }
}
