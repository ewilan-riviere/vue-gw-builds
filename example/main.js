import Vue from 'vue'
import App from './App.vue'
import vueGwBuilds from '../index'

Vue.config.productionTip = false

Vue.use(vueGwBuilds, {
  apiUrl: 'http://gwbbcode.arnapou.net/gwbbcode/api.js',
  lang: 'en'
})

new Vue({
  render: (h) => h(App)
}).$mount('#app')
